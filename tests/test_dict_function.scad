use <../openscad/libs/libdict.scad>;

test_dict = [["a", 1],
             ["b", 2],
             ["c", 3]];

assert(dict_lookup("a", test_dict) == 1, "Lookup of test_dict['a'] failed");


// convert a list-of-pairs to a closure style dictionary
function dict_to_lookup_function(dict) = 
    function(key=undef, value=undef) 
        is_undef(key) ? 
            dict : 
            is_undef(value) ? 
                dict_lookup(key, dict) :
                dict_to_lookup_function(dict_replace_value(key, value, dict));

test_func = dict_to_lookup_function(test_dict);

assert(test_func("a") == dict_lookup("a", test_dict), "Lookup with function failed");
assert(dict_valid(test_func()), "dict returned incorrectly by function");


modified_func = test_func("c", 4);
assert(modified_func("c") == 4, "Modify with function failed");
assert(test_func("c") == 3, "Lookup with function failed");

m = modified_func;
assert(m("c") == 4, "Modify with renamed function failed");

echo(m("a",5)("b",6)());


echo("Tests complete");