/*

This file defines a four bar mechanism.  Care has been taken to align
the flexures with the axis of intended load, to maximise stiffness
and flexibility by allowing longer flexures.

The flexure is defined as printed, with the fixed part along the X 
axis, and the tilting members along y.  The anchor sits on the +y
side of the XZ plane, such that the flexures are centred on y=0.

The flexure is centred at z=0, so the bounding box is +/- height/2.


Released under CERN-OHL-S-v2 by Richard Bowman, 2020

*/

use <../libs/libdict.scad>; //prefix dict_
use <../libs/libofu.scad>; //prefix ofu_

function parallelogram_flexure_default_params() = [
    ["anchor_width", 30],
    ["crank_length", 75],
    ["outer_width", 50],
    ["height", 6],
    ["strut_width", 6],
    ["xflex", [6, 3, 0.75]], //dimensions of a leafspring bending around x axis (i.e. moves up and down in the yz plane)
    ["zflex", [0.8, 4, 6]]   //same as xflex, but for a flexure that bemds around the z axis (moves in the xy plane)
];

// The y position of the anchor closest to the flexure
function parallelogram_flexure_anchor_y1(params) = 
    let(xflex = dict_lookup("xflex", params))
    xflex[1]/2;

// The y position if the anchor furthest from the flexure
function parallelogram_flexure_anchor_y2(params) = 
    let(y1 = parallelogram_flexure_anchor_y1(params),
        strut_width = dict_lookup("strut_width", params))
    y1 + strut_width;

module parallelogram_flexure_anchor(params){
    // The static anchor, to which the other parts attach
    y1 = parallelogram_flexure_anchor_y1(params);
    y2 = parallelogram_flexure_anchor_y2(params);
    size = [dict_lookup("anchor_width", params),
            y2 - y1,
            dict_lookup("height", params)];

    translate([-size[0]/2, y1, -size[2]/2]) cube(size);
}

// The y position of the carriage closest to the flexure
function parallelogram_flexure_carriage_y2(params) = 
    let(xflex=dict_lookup("xflex", params),
        crank_length=dict_lookup("crank_length", params))
    crank_length - xflex[1]/2;

// The y position if the anchor furthest from the flexure
function parallelogram_flexure_carriage_y1(params) = 
    let(y2 = parallelogram_flexure_carriage_y2(params),
        strut_width = dict_lookup("strut_width", params))
    y2 - strut_width;

module parallelogram_flexure_carriage(params){
    // The static anchor, to which the other parts attach
    y1 = parallelogram_flexure_carriage_y1(params);
    y2 = parallelogram_flexure_carriage_y2(params);
    size = [dict_lookup("anchor_width", params),
            y2 - y1,
            dict_lookup("height", params)];

    translate([-size[0]/2, y1, -size[2]/2]) cube(size);
}

// Calculate the minimum y extent - i.e. the bottom of the first rollbar
function parallelogram_flexure_outer_y1(params) =
    let(xflex = dict_lookup("xflex", params),
        zflex = dict_lookup("zflex", params),
        strut_width = dict_lookup("strut_width", params))
    min(-xflex[1]/2, -zflex[1]/2) - strut_width;

module parallelogram_flexure_rollbar_1(params){
    // Strut that rotates around the X axis only, connected to the anchor
    // NB this sits at y<0, with the pivot at y=0
    anchor_width = dict_lookup("anchor_width", params);
    outer_width = dict_lookup("outer_width", params);
    strut_width = dict_lookup("strut_width", params);
    h = dict_lookup("height", params);
    xflex = dict_lookup("xflex", params);
    zflex = dict_lookup("zflex", params);

    // inner_y2 connects via flexures to the anchor
    inner_y2 = - xflex[1]/2;
    // outer_y2 connects via z flexures to the cranks
    outer_y2 = - zflex[1]/2;
    y1 = parallelogram_flexure_outer_y1(params);
    
    ofu_reflect([1,0,0]) ofu_sequential_hull(){
        translate([-ofu_tiny()*2, y1, -h/2]) cube([ofu_tiny(), inner_y2 - y1, h]);
        translate([anchor_width/2, y1, -h/2]) cube([ofu_tiny(), inner_y2 - y1, h]);
        translate([outer_width/2 - strut_width, y1, -h/2]) cube([ofu_tiny(), outer_y2 - y1, h]);
        translate([outer_width/2, y1, -h/2]) cube([ofu_tiny(), outer_y2 - y1, h]);
    }
}

module parallelogram_flexure_rollbar_2(params){
    // Strut that rotates around the X axis only, connected to the anchor
    crank_length = dict_lookup("crank_length", params);
    
    translate([0, crank_length, 0]) mirror([0,1,0]) parallelogram_flexure_rollbar_1(params);
}

module parallelogram_flexure_crank_2(params){
    // Strut connecting the two rollbars, in y>0
    strut_width = dict_lookup("strut_width", params);
    x1 = dict_lookup("outer_width", params)/2 - strut_width;
    crank_length = dict_lookup("crank_length", params);
    zflex = dict_lookup("zflex", params);
    h = dict_lookup("height", params);

    translate([x1, zflex[1]/2, -h/2]) cube([strut_width, crank_length - zflex[1], h]);
}

module parallelogram_flexure_crank_1(params){
    // as above, y<0
    mirror([1,0,0]) parallelogram_flexure_crank_2(params);
}

module parallelogram_flexure_flexures(params){
    // the actual thin flexure bits
    crank_length = dict_lookup("crank_length", params);
    anchor_width = dict_lookup("anchor_width", params);
    outer_width = dict_lookup("outer_width", params);
    strut_width = dict_lookup("strut_width", params);
    h = dict_lookup("height", params);
    xflex = dict_lookup("xflex", params);
    zflex = dict_lookup("zflex", params);

    ofu_reflect([1,0,0]) ofu_repeat([0, crank_length, 0], 2, center=false){
        // flexures between anchor/carriage and rollbars
        translate([anchor_width - xflex[0], 0, 0]/2){
            cube(xflex + [0,1, 0], center=true);
        }
        // between cranks and rollbars
        translate([outer_width - strut_width - zflex[0], 0, 0]/2){
            cube(ofu_zeroz(zflex) + [0,1,h], center=true);
        }
    }
}

module parallelogram_flexure(params){
    union(){
        parallelogram_flexure_anchor(params);
        parallelogram_flexure_carriage(params);
        parallelogram_flexure_rollbar_1(params);
        parallelogram_flexure_rollbar_2(params);
        parallelogram_flexure_crank_1(params);
        parallelogram_flexure_crank_2(params);

        parallelogram_flexure_flexures(params);
    }
}

parallelogram_flexure(parallelogram_flexure_default_params());