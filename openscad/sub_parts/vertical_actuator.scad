/*
A vertical actuator for a delta-bot stage.

(c) Richard Bowman 2019
Released under CERN Open Hardware License
*/
use <../libs/libofu.scad>; //functions/modules prefixed with ofu_

aflex = [4, 3, 0.65]; // dimensions of the thin part of the main flexures used in the actuator.  Bends around the x-axis
flex_a = 0.2; // angle through which the flexure may be bent in radians (I mostly use 1.5mm long flexures, bent through 0.1 rad)
lever_l = 15; // the effective lever length.  Travel is +/- angle * 2 * lever_l (because there are two levers)
              // NB the actual lever will be lever_l - aflex[1], and the distance between the carriages will be
              // lever_l + aflex[1] because we approximate that the flexure hinges bend about a point in the middle.
lever_t = 4;  // thickness of the levers
column_bottom_block = [30, 8, 4]; // size of the bottom of the actuator column
column_top_block = [18,8, lever_t]; // size of the top of the actuator column
column_h = 30; // overall height of the actuator column
column_dz = column_h - column_top_block[2];
column_travel = lever_l * 2 * flex_a; // travel (of the actuator column)
lever_dz = lever_t + column_travel + 1; // clearance between the levers so they don't clash
lever_dy = 3.5; // offset in Y direction between the column and casing levers
column_zs = [-column_travel, 0, column_travel];
d = 0.05;
bridge_dz = 0.5;

module grow(shape="cylinder", r=1.5, h=4, dz=0, dy=1.5){
    // grow a shape by taking the minkowski product with a cylinder (or sphere)
    minkowski(){
        union() children();
        translate([0,0,dz]) if(shape=="cylinder"){
            cylinder(r=r, h=h, $fn=8, center=true);
        }else if(shape=="twocylinder"){
            repeat([0,dy,0], 2, center=true) cylinder(r=r, h=h, $fn=8, center=true);
        }else{
            sphere(r=r, $fn=8);
        }
    }
}
module yz_skew_about_y(pivot_y, skew){
    // skew the object such that the z += (y-pivot_y)*skew
    translate([0,pivot_y,0]) multmatrix([[1,0,0],[0,1,0],[0,skew,1]]) translate([0,-pivot_y,0]) children();
}

module actuator_column(){
    // The part of the actuator that moves vertically.  
    // In time, this should include the nut seat and elastic band mount
    ofu_sequential_hull(){
        translate(-ofu_zeroz(column_bottom_block)/2) 
            cube(column_bottom_block);
        translate([0, 0, column_dz - column_travel] - ofu_zeroz(column_top_block)/2) 
            cube(ofu_zeroz(column_top_block) + [0,0,ofu_tiny()]);
        translate([0, 0, column_dz] - ofu_zeroz(column_top_block)/2) 
            cube(column_top_block);
    }
}
// Each module in this file should have a "frame" module (that moves it according to the actuator's z position) and
// a "locus" module that creates copies at the extremes of the motion, so we can calculate clearances automatically.
module actuator_column_shift(z=0){
    // Translate the actuator column as if it was moving
    translate([0,0,z]) children();
}
module actuator_column_locus(){
    // Translate the actuator column through its travel
    for(z=column_zs) actuator_column_shift(z) children();
}

// There are four levers, connecting the column to the half stage, and the casing to the half stage:
// The first ones connect the actuator column to the "half stage" (perhaps I should call it the idler?)
module column_to_half_stage_lower(){
    translate([-column_bottom_block[0]/2, column_bottom_block[1]/2 + aflex[1], bridge_dz]) 
            cube([column_bottom_block[0], lever_l - aflex[1], lever_t - bridge_dz]);
}
module column_to_half_stage_upper(){
    translate([-column_top_block[0]/2, column_top_block[1]/2 + aflex[1], column_dz + bridge_dz]) 
            cube([column_top_block[0], lever_l - aflex[1], lever_t - bridge_dz]);
}
module column_to_half_stage_shift(z=0){
    // if we skew about the edge of the column, that should do it.
    yz_skew_about_y(column_bottom_block[1]/2 + aflex[1]/2 + 2*lever_l, -z/lever_l/2) children();
}
module column_to_half_stage_locus(){
    for(z=column_zs) column_to_half_stage_shift(z) children();
}
// These levers connect the "half stage" to the fixed world
module casing_to_half_stage_lower(){
    translate([0, lever_dy, lever_dz]) column_to_half_stage_lower();
}
module casing_to_half_stage_upper(){
    translate([0, lever_dy, lever_dz]) column_to_half_stage_upper();
}
module casing_to_half_stage_shift(z=0){
    // if we skew about the edge of the column, that should do it.
    yz_skew_about_y(column_bottom_block[1]/2 + aflex[1]/2 + lever_dy, z/lever_l/2) children();
}
module casing_to_half_stage_locus(){
    for(z=column_zs) casing_to_half_stage_shift(z) children();
}

module casing_to_half_stage_anchors(){
    translate([-column_bottom_block[0]/2-2, column_bottom_block[1]/2 + 1.5, lever_dz - 0.5])
            cube([column_bottom_block[0] + 4, lever_dy - 1.5, lever_t]);
    translate([-column_top_block[0]/2-2, column_top_block[1]/2 + 1.5, column_dz + lever_dz - 0.5]) 
            cube([column_top_block[0] + 4, lever_dy - 1.5, lever_t]);
}


module half_stage(){
    // This part moves parallel to the actuator column, but only goes about half as far.
    bottom_pos = [-column_bottom_block[0]/2, column_bottom_block[1]/2 + lever_l + aflex[1], 0];
    top_pos = [-column_top_block[0]/2, column_top_block[1]/2 + lever_l + aflex[1], column_dz];
    bottom_face = ofu_zeroz(column_bottom_block);
    top_face = ofu_zeroz(column_top_block);
    ofu_sequential_hull(){
        translate(bottom_pos) cube(column_bottom_block);
        translate(bottom_pos + [0, lever_dy, lever_dz]) cube(bottom_face + [0, -lever_dy, ofu_tiny()]);
        translate(bottom_pos + [0, lever_dy, lever_dz + lever_t]) cube(bottom_face + [0, -lever_dy, ofu_tiny()]);
        translate(top_pos) cube(top_face + [0,0,ofu_tiny()]);
        translate(top_pos) cube(column_top_block);
        translate(top_pos + [0, lever_dy, lever_dz]) cube(top_face + [0, -lever_dy, ofu_tiny()]);
        translate(top_pos + [0, lever_dy, lever_dz]) cube(column_top_block + [0, -lever_dy, ofu_tiny()]);
    }
}
module half_stage_shift(z=0){
    translate([0, lever_l*(cos(asin(z/2/lever_l)) - 1), z/2]) children();
}
module half_stage_locus(){
    for(z=column_zs) half_stage_shift(z) children();
}

// We draw in the flexures separately.  Generally, they are not included in the locus/other models, and
// as they distort when everything moves, there's not much point in animating them as a function of z.
module actuator_flexures(){
    // The two sets of levers (column_to_half_stage and column_to_casing) are identical, repeated at different positions.
    ofu_repeat([0, lever_dy, lever_dz], 2, center=false){
        // Either end of column_to_half_stage_lower
        ofu_reflect([1,0,0]) translate([-column_bottom_block[0]/2, column_bottom_block[1]/2 -ofu_tiny(), 0]) cube(aflex + [0, lever_l + 2*ofu_tiny(), 0]);
        // Either end of column_to_half_stage_upper
        ofu_reflect([1,0,0]) translate([-column_top_block[0]/2, column_top_block[1]/2 -ofu_tiny(), column_dz]) cube(aflex + [0, lever_l + 2*ofu_tiny(), 0]);
    }
}


module actuator_column_and_anchors(z){
    actuator_column_shift(z) actuator_column();
    column_to_half_stage_shift(z){ 
        column_to_half_stage_upper();
        column_to_half_stage_lower();
    }
    casing_to_half_stage_shift(z){
        casing_to_half_stage_upper();
        casing_to_half_stage_lower();
    }
    half_stage_shift(z) half_stage();
    casing_to_half_stage_anchors();
}

module actuator_column_void(){
    // This describes the volume that needs to be left clear for the
    // innards of the actuator to move up and down.  We use it to
    // hollow out the casing.
    union(){
        minkowski(){
            // Grow the void enough that we include space for the flexures
            //NB we need to be just bigger than the flexures, so the volume intersects 
            //with the volume below for the actuator column and half stage
            hull(){
                cube([2, 2*aflex[1]+2*ofu_tiny(), 2*ofu_tiny()], center=true);
                cube([ofu_tiny(), ofu_tiny(), 3], center=true);
            }
            union(){
                hull() column_to_half_stage_locus() column_to_half_stage_upper();
                hull() column_to_half_stage_locus() column_to_half_stage_lower();
                hull() casing_to_half_stage_locus() casing_to_half_stage_upper();
                hull() casing_to_half_stage_locus() casing_to_half_stage_lower();
            }
        }
        minkowski(){
            // Grow by a smaller amount here - no need for flexure clearance
            hull(){
                cube([2, 3, 2*ofu_tiny()], center=true);
                cube([ofu_tiny(), ofu_tiny(), 3], center=true);
            }
            union(){
                hull() actuator_column_locus() actuator_column();
                hull() half_stage_locus() half_stage();
            }
        }
    }
}

module actuator_casing(){
    // A casing that fits the actuator mechanism
    difference(){
        minkowski(){
            hull() actuator_column_void();
            cylinder(r1=1.3, r2=0, h=2, $fn=12);
        }
        actuator_column_void(); // leave space inside for the mechanism

        mirror([0,0,1]) cylinder(r=999, h=999, $fn=3); // cut off below z=0

        // cut off the outside of the actuator column so we can attach the parallelogram
        mirror([0,1,0]) mirror([0,0,1]) hull(){
            translate([-999,0,-column_h+5]) cube(9999);
            translate([-999,99,-column_h+5-99]) cube(9999);
        }
    }
    
}

//z = column_travel;
z=0;
render(6) actuator_column_and_anchors(z);
if(z==0){
    render(6) actuator_flexures();
}
difference(){
    # render(6) actuator_casing();
    //rotate([0,90,0]) cylinder(r=999,h=999,$fn=3);
}